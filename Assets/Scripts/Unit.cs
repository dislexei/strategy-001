﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Unit : MonoBehaviour {

    public int unitX;
    public int unitY;

    public TileMap tileMap;

    public List<Node> currentPath = null;

    private void Update()
    {
        if (currentPath != null) {
            int currentNode = 0;  
            while (currentNode < currentPath.Count -1)
            {
                Vector3 startLine = tileMap.TileCoordToWorldCoord(currentPath[currentNode].x, currentPath[currentNode].y) + new Vector3 (0, 0, -1f);
                Vector3 endLine = tileMap.TileCoordToWorldCoord(currentPath[currentNode+1].x, currentPath[currentNode+1].y) + new Vector3(0, 0, -1f);

                Debug.Log($"Drawing Line between {startLine.x}, {startLine.y} and {endLine.x}, {endLine.y}");

                Debug.DrawLine(startLine, endLine, Color.red);
                currentNode++;
            }
        }
    }


    public IEnumerator Move()
    {
        Debug.Log($"Starting to travel");
        while (currentPath.Count > 0)
        {
            
            Debug.Log($"Steps left: {currentPath.Count}");
            Node nextNode = currentPath.First();
            transform.position = new Vector3(nextNode.x, nextNode.y, gameObject.transform.position.z);
            this.unitX = nextNode.x;
            this.unitY = nextNode.y;
            currentPath.Remove(nextNode);
            yield return new WaitForSeconds(0.5f);
        }
        Debug.Log($"Arrived to destination");

    }
}
