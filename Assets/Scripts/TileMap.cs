﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TileMap : MonoBehaviour {

    public GameObject selectedUnit;

    public TileType[] tileTypes;
    Node[,] graph;

    List<Node> currentPath = null;

    int[,] tiles;

    int mapSizeX = 10;
    int mapSizeY = 10;

    void Start()
    {
        selectedUnit.GetComponent<Unit>().tileMap = this;
        selectedUnit.GetComponent<Unit>().unitX = (int)selectedUnit.transform.position.x;
        selectedUnit.GetComponent<Unit>().unitY = (int)selectedUnit.transform.position.y;

        GenerateMapData();
        GeneratePathfindingGraph();
        GenerateMapVisual();
    }


    void GenerateMapData()
    {
        // allocate map tiles
        tiles = new int[mapSizeX, mapSizeY];

        // initialize all tiles to grass
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                tiles[x, y] = 0;
            }
        }

        // let's make a U shaped mountain
        tiles[4, 4] = 2;
        tiles[5, 4] = 2;
        tiles[6, 4] = 2;
        tiles[7, 4] = 2;
        tiles[8, 4] = 2;
        tiles[4, 5] = 2;
        tiles[4, 6] = 2;
        tiles[8, 5] = 2;
        tiles[8, 6] = 2;
        // swamp
        tiles[4, 3] = 1;
        tiles[5, 3] = 1;
        tiles[6, 3] = 1;
        tiles[4, 2] = 1;
        tiles[5, 2] = 1;
        tiles[6, 2] = 1;
        tiles[4, 1] = 1;
        tiles[5, 1] = 1;
        tiles[6, 1] = 1;


    }


    //populate the graph
    void GeneratePathfindingGraph()
    {
        graph = new Node[mapSizeX, mapSizeY];

        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {

                graph[x, y] = new Node
                {
                    x = x,
                    y = y
                };
                graph[x, y].neighbours = new List<Node>();

            }

        }
        Debug.Log("Graph generated");
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {

                //getting nodes to be self aware of their location
                //TODO: this is a bit awful, refactor
             
                //Debug.Log($"Current: {x} , {y}");
                //4-way
                if (x > 0) {
                    graph[x, y].neighbours.Add(graph[x - 1, y]);
                }
                if (x < mapSizeX - 1)
                {
                    graph[x, y].neighbours.Add(graph[x + 1, y]);
                }
                if (y > 0)
                {
                    graph[x, y].neighbours.Add(graph[x, y - 1]);
                }
                if (y < mapSizeY - 1)
                {
                    graph[x, y].neighbours.Add(graph[x, y + 1]);
                }
            }
        }
        Debug.Log("Neighbours populated");
    }

    void GenerateMapVisual()
    {
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                TileType tt = tileTypes[tiles[x, y]];
                GameObject go = (GameObject)Instantiate(tt.tileVisualPrefab, new Vector3(x, y, 0), Quaternion.identity);
                TileClickHandler tch = go.GetComponent<TileClickHandler>();

                tch.tileX = x;
                tch.tileY = y;

                tch.map = this;
            }
        }
    }

    // for future translation
    public Vector3 TileCoordToWorldCoord(int x, int y) {

        return new Vector3(x, y, 0);
    }

    private float GetDistanceBetweenNeighbours(int x, int y)
    {
        int tileType = tiles[x, y];
        float cost = tileTypes[tileType].cost;
        return cost;
    }

    public void GetPathToTile (int x, int y)
    {
        selectedUnit.GetComponent<Unit>().currentPath = null;
        Debug.Log($"Getting path to {x}, {y}..");
        currentPath = null;

        //// creating and setting all nodes to unvisited
        //List<Node> unvisitedNodes = new List<Node>();
        //foreach (Node node in graph)
        //{
        //    node.distance = Mathf.Infinity;
        //    unvisitedNodes.Add(node);
        //}

        //// determining the start and end positions for pathfinding
        //Node start = graph[selectedUnit.GetComponent<Unit>().unitX, selectedUnit.GetComponent<Unit>().unitY];
        //Node destination = graph[x, y];

        //// setting the current node to starting position; setting its distance to zero
        //Node current = start;
        //start.distance = 0;

        //// consider distance to all unvisited neighbours of the current node, compare to current distance and assign the smaller value 

        //Debug.Log($"The distance to clicked tile is {GetDistanceBetweenNeighbours(x, y)}");

        Dictionary<Node, float> dist = new Dictionary<Node, float>();
        Dictionary<Node, Node> prev = new Dictionary<Node, Node>();
        List<Node> unvisitedNodes = new List<Node>();

        Node start = graph[selectedUnit.GetComponent<Unit>().unitX, selectedUnit.GetComponent<Unit>().unitY];
        Node target = graph[x, y];

        Debug.Log($"Start = {selectedUnit.GetComponent<Unit>().unitX},{selectedUnit.GetComponent<Unit>().unitY}; Target = {x},{y}");

        dist[start] = 0;
        prev[start] = null;


        // populating unvisited node list;
        // setting all distances apart from start to null;
        // setting all previous nodes to null
        foreach (Node node in graph)
        {
            if (node != start) {
                dist[node] = Mathf.Infinity;
                prev[node] = null;
            }
            unvisitedNodes.Add(node);
        }

        Debug.Log($"Considering {unvisitedNodes.Count} nodes");

        while(unvisitedNodes.Count > 0)
        {
            //not fast, gotta learn how to use Min in this case
            //picking the node with minimal distance
            Node u = unvisitedNodes.OrderBy(n => dist[n]).First();

            //break out of the while loop when target is found
            if (u == target)
            {
                break;
            }
            unvisitedNodes.Remove(u);

            foreach (Node v in u.neighbours) {

                //euclidian distance
                float alt = dist[u] + u.DetermineDistanceTo(v);

                //subjective game distance (cost)
                alt = alt * GetDistanceBetweenNeighbours(v.x, v.y);

                //Debug.Log($"distance = {alt}");

                if (alt < dist[v])
                {
                    dist[v] = alt;
                    prev[v] = u;
                }
            }
        }

        Debug.Log($"Nodes remaining = {unvisitedNodes.Count}");

        //if we got here, we've found the shortest path,
        // or there is not path

        //if (prev[target] == null)
        //{
        //    Debug.Log("There is no path");
        //    //no route
        //    return;
        //}

        currentPath = new List<Node>();

        Node curr = target;
        //Node curr = start;

        //step through the prev chain and add it to our path
        while (curr != null)
        {
            currentPath.Add(curr);
            curr = prev[curr];
        }
        //now right now the currentPath describes reversed path to target
        currentPath.Reverse(); 

        Debug.Log($"Path built through {currentPath.Count} nodes.");

        foreach(Node n in currentPath)
        {
            int step = 1;
            Debug.Log($"Step = {step}, coord = {n.x},{n.y}");
        }

        selectedUnit.GetComponent<Unit>().currentPath = currentPath;
        selectedUnit.GetComponent<Unit>().StartCoroutine("Move");
    }
}


public class Node
{

    public List<Node> neighbours;
    public float distance;

    public int x;
    public int y;

    public Node()
    {
        List<Node> neighbours = new List<Node>();
    }


    //TODO currently returns Euclidian distance, refactor to return movement cost
    public float DetermineDistanceTo(Node next)
    {
        float euclidianDistance = Vector2.Distance(new Vector2(this.x, this.y), new Vector2(next.x, next.y));
        //float nextNodeCost = tiles[next.x, next.y];

        return Vector2.Distance(new Vector2(this.x, this.y), new Vector2(next.x, next.y));
    }
}
