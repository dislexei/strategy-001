﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TileType {

    public string name;
    public GameObject tileVisualPrefab;

    //to use in pathfinding
    public float cost;

    //public bool isWalkable;

}
